//
//  PhotoCollectionViewCell.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/26/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit

class PhotoCVC: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var entryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        
        setNeedsLayout()
        layoutIfNeeded()
        
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        
        var frame = layoutAttributes.frame
        frame.size.height = ceil(size.height) + 50
        layoutAttributes.frame = frame
        
        
        return layoutAttributes
    }
}
