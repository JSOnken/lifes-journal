//
//  CalendarCell.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/13/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarCell: JTAppleCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var entryFoundView: UIView!
}
