//
//  TextCollectionViewCell.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/20/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit

class TextCVC: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var entryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        
        setNeedsLayout()
        layoutIfNeeded()
        
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        
        var frame = layoutAttributes.frame
        frame.size.height = ceil(size.height)
        layoutAttributes.frame = frame
        
        
        return layoutAttributes
    }
}
