//
//  AppDelegate.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/13/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import FacebookCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var signedIn: Bool?
    var storyboard: UIStoryboard?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let center = UNUserNotificationCenter.current();
        let options: UNAuthorizationOptions = [.alert, .sound]
        
        center.requestAuthorization(options: options) { (granted, error) in
            if !granted {
                if let error = error {
                    print(error.localizedDescription)
                }
            }
        }
        
        center.getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                print("Not authorized to send notifications")
            }
        }
        
        let content = UNMutableNotificationContent();
        content.title = "Let's write"
        content.body = "Make sure to write in your journal every day!"
        content.sound = UNNotificationSound.default()
        
        var dateComponents = DateComponents()
        dateComponents.hour = 8
        dateComponents.minute = 30
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let identifier = "UYLLocalNotification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        center.add(request) { (error) in
            if let error = error {
                print(error.localizedDescription)
            }
        }
        
        FirebaseApp.configure()
        let db = Firestore.firestore()
        
        print(db, "Database!!!!")
        
        if Auth.auth().currentUser != nil {
            signedIn = true
        } else {
            signedIn = false
        }
        
        self.storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        if signedIn != false {
            self.window?.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController
        }
        
        let defaults = UserDefaults.standard
        let accentColor = ["AccentColor":"green"]
        let fontStyle = ["FontStyle":""]
        defaults.register(defaults: accentColor)
        defaults.register(defaults: fontStyle)
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return SDKApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
