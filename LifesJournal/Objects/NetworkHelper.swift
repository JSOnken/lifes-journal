//
//  NetworkHelper.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/22/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import UIKit

class NetworkHelper: NSObject {
    
    let reachability = Reachability()!
    
    static let sharedInstance: NetworkHelper = { return NetworkHelper() }()
    
    func startObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    func isConnected() -> Bool {
        
        switch reachability.connection {
        case .wifi, .cellular:
            return true
        case .none:
            return false
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
        case .cellular:
            print("Reachable via Cellular")
        case .none:
            print("Network not reachable")
        }
    }
    
    func stopObserver() {
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
    
    func showNetworkAlert(view: UIViewController) {
        let alertController = UIAlertController(title: "No Network!", message: "No network connection found. Please check your device's network connection. Life's Journal will use cached data until a network is connected.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        view.present(alertController, animated: true, completion: nil)
    }
}
