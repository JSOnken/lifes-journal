//
//  DatabaseHelper.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/4/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import Firebase


class DatabaseHelper {
    
    var db: Firestore!
    var docRef: DocumentReference!
    var entriesArray = [JournalEntry]()
    var userEmail: String?
    
    static let sharedInstance: DatabaseHelper = { return DatabaseHelper() }()
    
    func getDatabaseEntries(database: Firestore) -> Array<JournalEntry> {
        
        db = database
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            let user = Auth.auth().currentUser
            if let user = user {
                userEmail = user.email
            }
            print("\(userEmail!) is signed in")
        }
        
        db.collection("users").document(userEmail!).collection("journal entries").getDocuments { (snapshot, error) in
            if let error = error {
                print("Error getting documents: \(error)")
            } else {
                for document in snapshot!.documents {
                    print("\n")
                    let docId = document.documentID
                    let textEntry = document.get("entry") as? String
                    let downloadURL = document.get("downloadURL") as? String
                    let audioURL = document.get("audioURL") as? String
                    let latitude = document.get("latitude") as? Double
                    let longitude = document.get("longitude") as? Double
                    let journalEntry = JournalEntry.init(dateEntered: docId, textEntry: textEntry, downloadURL: downloadURL, audioURL: audioURL, latitude: latitude, longitude: longitude)
                    self.entriesArray.append(journalEntry)
                } // end for loop
            } // end if statement
        } // end db closure
        
        return entriesArray
    } // end function
    
    func getCachedEntries() -> Array<JournalEntry> {
        return entriesArray
    }
    
    func setCachedentries(entries: Array<JournalEntry>) {
        entriesArray = entries
    }
}
