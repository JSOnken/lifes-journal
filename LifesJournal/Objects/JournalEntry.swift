//
//  JournalEntry.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/28/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation

class JournalEntry {
    
    var dateEntered: String?
    var textEntry: String?
    var photoURL: String?
    var audioURL: String?
    var latitude: Double?
    var longitude: Double?
    
    init(dateEntered: String?, textEntry: String?, downloadURL: String?, audioURL: String?, latitude: Double?, longitude: Double?) {
        self.dateEntered = dateEntered
        self.textEntry = textEntry
        self.photoURL = downloadURL
        self.audioURL = audioURL
        self.latitude = latitude
        self.longitude = longitude
    }
}
