//
//  HandwritingCanvas.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/13/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import UIKit

public class HandwritingCanvas: UIImageView {
    
    let pi = CGFloat(Double.pi)
    let forceSensitivity: CGFloat = 4.0
    let defaultLineWidth: CGFloat = 6.0
    
    public override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        
        image?.draw(in: bounds)
        var touches = [UITouch]()
        
        if let coalescedTouches = event?.coalescedTouches(for: touch) {
            touches = coalescedTouches
        } else {
            touches.append(touch)
        }
        
        for touch in touches {
            drawStroke(context: context, touch: touch)
        }
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    func drawStroke(context: CGContext?, touch: UITouch) {
        let previousLocation = touch.previousLocation(in: self)
        let location = touch.location(in: self)
        
        var lineWidth: CGFloat = 1.0
        
        if touch.type == .pencil {
            lineWidth = lineWidthForDrawing(context: context, touch: touch)
            UIColor.darkGray.setStroke()
        }
        
        context!.setLineWidth(lineWidth)
        context!.setLineCap(.round)
        
        context?.move(to: previousLocation)
        context?.addLine(to: location)
        
        context!.strokePath()
    }
    
    func lineWidthForDrawing(context: CGContext?, touch: UITouch) -> CGFloat {
        var lineWidth = defaultLineWidth
        
        if touch.force > 0 {
            lineWidth = touch.force + forceSensitivity
        }
        
        return lineWidth
    }
    
    func clearCanvas(_ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.5, animations: {
                self.alpha = 0
            }, completion: { finisehd in
                self.alpha = 1
                self.image = nil
            })
        } else {
            image = nil
        }
    }
}
