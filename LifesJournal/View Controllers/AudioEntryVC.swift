//
//  AudioEntryViewController.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/26/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation
import Firebase

class AudioEntryVC: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate, CLLocationManagerDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var entryTextView: UITextField!
    
    // MARK: - Variables
    var db: Firestore!
    let networkHelper: NetworkHelper = NetworkHelper.sharedInstance
    let locationManager = CLLocationManager()
    var recorder: AVAudioRecorder!
    var player: AVAudioPlayer!
    var recording: Bool = false
    var playing: Bool = false
    var locationAuthorized: Bool?
    var currentLocation: CLLocation?
    var audioFileName: URL?
    var userEmail: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        db = Firestore.firestore()
        modalView.applyViewDesign()
        setupRecorder()
        playButton.isHidden = true
        
        checkLocationAuthorizationStatus()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            print("User is signed in")
            let user = Auth.auth().currentUser
            if let user = user {
                userEmail = user.email
            }
        } else {
            print("User is not signed in")
        }
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AudioEntryVC.dismissKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(AudioEntryVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AudioEntryVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        saveAudioFileToFirebase()
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func recordButtonPressed(_ sender: UIButton) {
        if recording == false {
            sender.setBackgroundImage(UIImage(named: "stop_button"), for: .normal)
            descriptionLabel.text = "Click to stop recording audio entry"
            recording = true
            recorder.record()
            playButton.isHidden = true
        } else {
            sender.setBackgroundImage(UIImage(named: "record_button"), for: .normal)
            descriptionLabel.text = "Click to start recording audio entry"
            recording = false
            recorder.stop()
            playButton.isEnabled = true
            playButton.isHidden = false
        }
    }
    
    @IBAction func playbuttonPressed(_ sender: UIButton) {
        if playing == false {
            sender.setBackgroundImage(UIImage(named: "small_stop_button"), for: .normal)
            setupPlayer()
            player.play()
            recordButton.isEnabled = false
            playing = true
        } else {
            player.stop()
            sender.setBackgroundImage(UIImage(named: "play_button"), for: .normal)
            recordButton.isEnabled = true
            playing = false
        }
    }
    
    func getDocumentDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getFileName() -> URL {
        let dateString = getDateTime()
        let path = getDocumentDirectory().appendingPathComponent("\(dateString).m4a")
        
        print("Audio path: \(path.absoluteString)")
        return path
    }
    
    func setupRecorder() {
        audioFileName = getFileName()
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
        } catch let error as NSError {
            print("Error setting up recorder: \(error.localizedDescription)")
        }
        
        let settings = [AVFormatIDKey: kAudioFormatAppleLossless,
                        AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue,
                        AVEncoderBitRateKey: 320000,
                        AVNumberOfChannelsKey: 2,
                        AVSampleRateKey: 44100.2] as [String : Any]
        
        do{
            try recorder = AVAudioRecorder(url: getFileName(), settings: settings)
            recorder.delegate = self
            recorder.prepareToRecord()
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func setupPlayer() {
        do {
            player = try AVAudioPlayer(contentsOf: audioFileName!)
            player.delegate = self
            player.prepareToPlay()
            player.volume = 3.0
        } catch {
            print(error)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        playButton.setBackgroundImage(UIImage(named: "play_button"), for: .normal)
        recordButton.isEnabled = true
        playing = false
    }
    
    func checkLocationAuthorizationStatus() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationAuthorized = true
            break
        case .denied:
            locationAuthorized = false
            break
        case .restricted:
            locationAuthorized = false
            break
        default:
            locationManager.requestWhenInUseAuthorization()
            break
        }
    }
    
    func getDateTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        let dateString = formatter.string(from: Date())
        return dateString
    }
    
    func saveAudioFileToFirebase() {
        
        if networkHelper.isConnected() {
            let storageRef = Storage.storage().reference(withPath: "/Audio/\(userEmail!)/\(getDateTime()).m4a")
            
            let uploadTask = storageRef.putFile(from: audioFileName!, metadata: nil) { (metaData, error) in
                guard let metaData = metaData else {
                    return
                }
                
                let size = metaData.size
                print("MetaData Size: \(size)")
                
                storageRef.downloadURL(completion: { (url, error) in
                    guard let downloadUrl = url?.absoluteString else {
                        return
                    }
                    
                    if self.locationAuthorized == false {
                        if let textEntry = self.entryTextView.text {
                            self.db.collection("users").document(self.userEmail!).collection("journal entries").document(self.getDateTime())
                                .setData(["entry":textEntry,
                                          "audioURL":downloadUrl], merge: true, completion: { (error) in
                                            if let error = error {
                                                print("Error writing document: \(error.localizedDescription)")
                                            } else {
                                                print("Document successfully written!")
                                                self.dismiss(animated: true)
                                            }
                                })
                        } else {
                            self.db.collection("users").document(self.userEmail!).collection("journal entries").document(self.getDateTime())
                                .setData(["audioURL":downloadUrl], merge: true, completion: { (error) in
                                    if let error = error {
                                        print("Error writing document: \(error.localizedDescription)")
                                    } else {
                                        print("Document successfully written!")
                                        self.dismiss(animated: true)
                                    }
                                })
                        }
                    } else if self.locationAuthorized == true {
                        if let textEntry = self.entryTextView.text {
                            self.db.collection("users").document(self.userEmail!).collection("journal entries").document(self.getDateTime())
                                .setData(["entry":textEntry,
                                          "latitude": self.currentLocation!.coordinate.latitude,
                                          "longitude": self.currentLocation!.coordinate.longitude,
                                          "audioURL":downloadUrl], merge: true, completion: { (error) in
                                            if let error = error {
                                                print("Error writing document: \(error.localizedDescription)")
                                            } else {
                                                print("Document successfully written!")
                                                self.dismiss(animated: true)
                                            }
                                })
                        } else {
                            self.db.collection("users").document(self.userEmail!).collection("journal entries").document(self.getDateTime())
                                .setData(["latitude": self.currentLocation!.coordinate.latitude,
                                          "longitude": self.currentLocation!.coordinate.longitude,
                                          "audioURL":downloadUrl], merge: true, completion: { (error) in
                                            if let error = error {
                                                print("Error writing document: \(error.localizedDescription)")
                                            } else {
                                                print("Document successfully written!")
                                                self.dismiss(animated: true)
                                            }
                                })
                        }
                    }
                    print("URL: \(downloadUrl)")
                })
            }
            
            // Update progress bar
            uploadTask.observe(.progress) { [weak self] (snapshot) in
                guard let progress = snapshot.progress else { return }
                self?.progressView.isHidden = false
                self?.progressView.progress = Float(progress.fractionCompleted)
                print(progress)
            }
        } else {
            networkHelper.showNetworkAlert(view: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locationValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        currentLocation = CLLocation(latitude: locationValue.latitude, longitude: locationValue.longitude)
    }
}

extension AudioEntryVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    @objc func dismissKeyboard() {
        entryTextView.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 200
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 200
            }
        }
    }
}
