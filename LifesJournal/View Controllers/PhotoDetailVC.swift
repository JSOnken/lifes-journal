//
//  PhotoDetailVC.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/30/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class PhotoDetailVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var entryTV: UITextView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var shareButton: UIButton!
    
    
    // MARK: - Properties
    var db: Firestore!
    let networkHelper: NetworkHelper = NetworkHelper.sharedInstance
    var selectedEntry: JournalEntry!
    var userEmail: String?
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        db = Firestore.firestore()
        
        entryTV.isEditable = false
        entryTV.text = selectedEntry.textEntry
        
        if networkHelper.isConnected() {
            let photoURL = URL(string: selectedEntry.photoURL!)
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: photoURL!)
                DispatchQueue.main.async {
                    self.photoImageView.image = UIImage(data: data!)
                }
            }
        } else {
            networkHelper.showNetworkAlert(view: self)
        }
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            let user = Auth.auth().currentUser
            if let user = user {
                userEmail = user.email
            }
        } else {
            print("User is not signed in")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadTheme()
        entryTV.font = Theme.shared.getFontStyle()
    }
    
    @IBAction func editButtonPressed(_ sender: Any) {
        editButton.isHidden = true
        saveButton.isHidden = false
        entryTV.isEditable = true
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        guard let textEntry = entryTV.text, !textEntry.isEmpty else {return}
        
        if networkHelper.isConnected() {
            db.collection("users").document(userEmail!).collection("journal entries").document(selectedEntry.dateEntered!)
                .setData(["entry":textEntry,
                          "downloadURL":selectedEntry.photoURL!], merge: true, completion: { (error) in
                            if let error = error {
                                print("Error writing document: \(error.localizedDescription)")
                            } else {
                                print("Document successfully written!")
                                self.dismiss(animated: true)
                            }
                })
        } else {
            networkHelper.showNetworkAlert(view: self)
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        if networkHelper.isConnected() {
            db.collection("users").document(userEmail!).collection("journal entries").document(selectedEntry.dateEntered!).delete { (error) in
                if let error = error {
                    print("Error deleting document: \(error.localizedDescription)")
                } else {
                    print("Document was successfully deleted")
                    self.dismiss(animated: true, completion: nil)
                }
            }
        } else {
            networkHelper.showNetworkAlert(view: self)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        let photoURL = URL(string: selectedEntry.photoURL!)
        
        let data = try? Data(contentsOf: photoURL!)
        let photo = UIImage(data: data!)
        
        let items = [selectedEntry.textEntry!, photo!] as [Any]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
        if let popOver = ac.popoverPresentationController {
            popOver.sourceView = self.shareButton
        }
    }
    
    func loadTheme() {
        customNavBar.backgroundColor = Theme.shared.getThemecolor()
    }
}
