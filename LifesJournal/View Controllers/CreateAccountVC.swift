//
//  CreateAccountViewController.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/17/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase
import ValidationComponents
import Hero

class CreateAccountVC: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var db: Firestore!
    var docRef: DocumentReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CreateAccountVC.dismissKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateAccountVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateAccountVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        createButton.applyButtonDesign()
        popupView.applyViewDesign()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
    }
    
    @IBAction func createButtonPressed(_ sender: Any) {
        
        let emailPredicate = EmailValidationPredicate()
        
        if emailTextField.text?.isEmpty ?? true || passwordTextField.text?.isEmpty ?? true || confirmPasswordTextField.text?.isEmpty ?? true {
            print("Something is empty")
            alertLabel.isHidden = false
        } else {
            if let email = emailTextField.text, let password = passwordTextField.text {
                if emailPredicate.evaluate(with: email) == true {
                    if passwordTextField.text == confirmPasswordTextField.text {
                        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
                            guard let email = authResult?.user.email, error == nil else {
                                print("user already created")
                                self.alertLabel.isHidden = false
                                self.alertLabel.text = "User already created"
                                return
                            }
                            print("\(email) created")
                            
                            //                            guard let user = authResult?.user else {
                            //                                return
                            //                            }
                            if let tabViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController {
                                
                                tabViewController.hero.isEnabled = true
                                
                                tabViewController.hero.modalAnimationType = .selectBy(presenting: .zoomOut, dismissing: .fade)
                                
                                self.present(tabViewController, animated: true, completion: nil)
                            }
                            
                        }
                    } else {
                        print("Password does not match")
                        alertLabel.text = "Passwords do not match"
                    } // end password if check
                } else {
                    print("Email not valid")
                    alertLabel.isHidden = false
                    alertLabel.text = "Email address is not valid"
                } // end evalute email if check
            } // end if let email and password check
        } // end empty check
    } // end create action
}

extension CreateAccountVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextTextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField{
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            
            return true
        }
        return false
    }
    
    @objc func dismissKeyboard() {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        confirmPasswordTextField.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 200
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 200
            }
        }
    }
}
