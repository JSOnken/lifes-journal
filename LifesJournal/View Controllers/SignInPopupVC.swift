//
//  SignInPopupViewController.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/17/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase
import Hero

class SignInPopupVC: UIViewController {
    
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SignInPopupVC.dismissKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(SignInPopupVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignInPopupVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        signUpButton.applyButtonDesign()
        popupView.applyViewDesign()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @IBAction func signInButtonPressed(_ sender: Any) {
        if emailTextField.text?.isEmpty ?? true || passwordTextField.text?.isEmpty ?? true {
            print("Something is empty")
            alertLabel.isHidden = false
        } else {
            if let email = emailTextField.text, let password = passwordTextField.text {
                Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                    if let error = error {
                        print(error.localizedDescription)
                        return
                    }
                    print("Sign in successful")
                    if let tabViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController {
                        
                        tabViewController.hero.isEnabled = true
                        tabViewController.hero.modalAnimationType = .zoom
                        
                        self.present(tabViewController, animated: true, completion: nil)
                    }
                } // end auth closure
            } // end else check
        } // end if check
    } // end function
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        if let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "createAccountVC") {
            vc2.hero.isEnabled = true
            
            vc2.hero.modalAnimationType = .zoom
            
            present(vc2, animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true)
    }
}

extension SignInPopupVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextTextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField{
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            
            return true
        }
        return false
    }
    
    @objc func dismissKeyboard() {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 200
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 200
            }
        }
    }
}
