//
//  AudioDetailVC.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/13/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation

class AudioDetailVC: UIViewController {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var modalView: UIView!
    
    var selectedEntry: JournalEntry!
    var db: Firestore!
    let networkHelper: NetworkHelper = NetworkHelper.sharedInstance
    var userEmail: String?
    var player: AVPlayer!
    var audioFileName: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        db = Firestore.firestore()
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            let user = Auth.auth().currentUser
            if let user = user {
                userEmail = user.email
            }
        } else {
            print("User is not signed in")
        }
        
        modalView.applyViewDesign()
        setupPlayer()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didPlayToEnd), name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    @IBAction func playButtonPressed(_ sender: UIButton) {
        if player.timeControlStatus == .playing {
            player.pause()
            sender.setBackgroundImage(UIImage(named: "play_button"), for: .normal)
        } else if player.timeControlStatus == .paused {
            player.play()
            sender.setBackgroundImage(UIImage(named: "pause_button"), for: .normal)
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        if networkHelper.isConnected() {
            db.collection("users").document(userEmail!).collection("journal entries").document(selectedEntry.dateEntered!).delete { (error) in
                if let error = error {
                    print("Error deleting document: \(error.localizedDescription)")
                } else {
                    print("Document was successfully deleted")
                    self.dismiss(animated: true, completion: nil)
                }
            }
        } else {
            networkHelper.showNetworkAlert(view: self)
        }
    }
    
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func setupPlayer() {
        do {
            let audioUrl = URL(string: selectedEntry.audioURL!)
            let playerItem = AVPlayerItem(url: audioUrl!)
            self.player = AVPlayer(playerItem: playerItem)
            player.volume = 3.0
        }
    }
    
    @objc func didPlayToEnd() {
        player.seek(to: CMTimeMakeWithSeconds(0, 1))
        playButton.setBackgroundImage(UIImage(named: "play_button"), for: .normal)
    }
}
