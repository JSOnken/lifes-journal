//
//  TextEntryViewController.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/18/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class TextEntryVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var journalEntryTextView: UITextView!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var canvasView: CanvasView!
    
    let locationManager = CLLocationManager()
    var docRef: DocumentReference!
    let networkHelper: NetworkHelper = NetworkHelper.sharedInstance
    var db: Firestore!
    var userID: String?
    var userEmail: String?
    var locationAuthorized: Bool?
    var currentLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        canvasView.isHidden = true
        
        checkLocationAuthorizationStatus()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
        
        docRef = Firestore.firestore().document("users/myCustomUser")
        db = Firestore.firestore()
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            print("User is signed in")
            let user = Auth.auth().currentUser
            if let user = user {
                userID = user.uid
                userEmail = user.email
            }
        } else {
            print("User is not signed in")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadTheme()
        journalEntryTextView.font = Theme.shared.getFontStyle()
        journalEntryTextView.setNeedsFocusUpdate()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if networkHelper.isConnected() {
            if canvasView.isHidden == false {
                let image = UIImage(view: canvasView)
                if let imageData = UIImageJPEGRepresentation(image, 0.8) {
                    let data = imageData as NSData
                    uploadImageToFirebaseStorage(data: data)
                }
                
            } else {
                uploadTextEntyToFirebaseStroage()
            }
        } else {
            networkHelper.showNetworkAlert(view: self)
        }
        
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func handwritingButtonPressed(_ sender: Any) {
        if canvasView.isHidden == true {
            canvasView.isHidden = false
            clearButton.isHidden = false
            self.view.endEditing(true)
        } else {
            canvasView.isHidden = true
            clearButton.isHidden = true
        }
    }
    
    @IBAction func clearButtonPressed(_ sender: Any) {
        canvasView.clearCanvas()
    }
    
    
    func getDateTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        let dateString = formatter.string(from: Date())
        return dateString
    }
    
    func checkLocationAuthorizationStatus() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationAuthorized = true
            break
        case .denied:
            locationAuthorized = false
            break
        case .restricted:
            locationAuthorized = false
            break
        default:
            locationManager.requestWhenInUseAuthorization()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locationValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        currentLocation = CLLocation(latitude: locationValue.latitude, longitude: locationValue.longitude)
    }
    
    func loadTheme() {
        customNavBar.backgroundColor = Theme.shared.getThemecolor()
    }
    
    func uploadTextEntyToFirebaseStroage() {
        guard let textEntry = journalEntryTextView.text, !textEntry.isEmpty else {return}
        
        if locationAuthorized == false {
            db.collection("users").document(userEmail!).collection("journal entries").document(getDateTime())
                .setData(["entry":textEntry], merge: true, completion: { (error) in
                    if let error = error {
                        print("Error writing document: \(error.localizedDescription)")
                    } else {
                        print("Document successfully written!")
                        self.dismiss(animated: true)
                    }
                })
        } else if locationAuthorized == true {
            db.collection("users").document(userEmail!).collection("journal entries").document(getDateTime())
                .setData(["entry":textEntry,
                          "latitude": currentLocation!.coordinate.latitude,
                          "longitude": currentLocation!.coordinate.longitude],
                         merge: true, completion: { (error) in
                            if let error = error {
                                print("Error writing document: \(error.localizedDescription)")
                            } else {
                                print("Document successfully written!")
                                self.dismiss(animated: true)
                            }
                })
        }
    }
    
    func uploadImageToFirebaseStorage(data: NSData) {
        
        let storageRef = Storage.storage().reference(withPath: "/Photos/\(userEmail!)/\(getDateTime()).jpg")
        let uploadMetaData = StorageMetadata()
        
        uploadMetaData.contentType = "image/jpg"
        
        let uploadTask = storageRef.putData(data as Data, metadata: uploadMetaData) { (metaData, error) in
            if let error = error {
                print("Error uploading photo: \(error.localizedDescription)")
            } else {
                // Get the download url from the image after it was uploaded
                storageRef.downloadURL(completion: { (url, error) in
                    // Pring the error if one is found
                    if let error = error {
                        print("Error getting URL: \(error.localizedDescription)")
                        // Get the download URL if one is found
                    } else {
                        if let downloadURL = url?.absoluteString {
                            print("URL: \(downloadURL)")
                            // If we do not have authorization to user location
                            if self.locationAuthorized == false {
                                // Set the data for the photo entry to the database without GPS location
                                self.db.collection("users").document(self.userEmail!).collection("journal entries").document(self.getDateTime())
                                    .setData(["downloadURL":downloadURL], merge: true, completion: { (error) in
                                        if let error = error {
                                            print("Error writing document: \(error.localizedDescription)")
                                        } else {
                                            print("Document successfully written!")
                                            self.dismiss(animated: true)
                                        }
                                    })
                                // If we do have authorization to user location
                            } else if self.locationAuthorized == true {
                                // Set the data for the photo entry to the database with GPS location
                                self.db.collection("users").document(self.userEmail!).collection("journal entries").document(self.getDateTime())
                                    .setData(["latitude": self.currentLocation!.coordinate.latitude,
                                              "longitude": self.currentLocation!.coordinate.longitude,
                                              "downloadURL":downloadURL], merge: true, completion: { (error) in
                                                if let error = error {
                                                    print("Error writing document: \(error.localizedDescription)")
                                                } else {
                                                    print("Document successfully written!")
                                                    self.dismiss(animated: true)
                                                }
                                    }) // end of db completion block
                            } // end of locationAuthorized if check
                        } // end of download url if let block
                    } // end of else block
                }) // end of downloadURL completion block
            }
        } // end of upload task
        
        // Update progress bar
        //        uploadTask.observe(.progress) { [weak self] (snapshot) in
        //            guard let progress = snapshot.progress else { return }
        //            self?.progressView.isHidden = false
        //            self?.progressView.progress = Float(progress.fractionCompleted)
        //        }
    }
}
