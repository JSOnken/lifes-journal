//
//  JournalViewController.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/20/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import AlamofireImage

private let textCellIdentifier = "TextEntryIdentifier"
private let photoCellIdentifier = "PhotoEntryIdentifier"
private let audioCellIdentifier = "AudioEntryIdentifier"
private let photoSegueIdentifier = "photoDetailSegueIdentifier"
private let textSegueIdentifier = "textDetailSegueIdentifier"
private let audioSegueIdentifier = "audioDetailSegueIdentifier"

class JournalVC: UIViewController, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Outlets
    @IBOutlet weak var journalCollectionView: UICollectionView!
    @IBOutlet weak var customNavBar: UIView!
    
    // MARK: - Properties
    var db: Firestore!
    let networkHelper: NetworkHelper = NetworkHelper.sharedInstance
    let databaseHelper: DatabaseHelper = DatabaseHelper.sharedInstance
    var entryCount = 0
    var entriesArray = [JournalEntry]()
    var userID: String?
    var userEmail: String?
    var lastOrientation:UIDeviceOrientation!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        let accentColor = defaults.string(forKey: "AccentColor")
        if let accentColor = accentColor {
            Theme.shared.setThemeColor(selectedColor: accentColor)
        }
        let fontStyle = defaults.string(forKey: "FontStyle")
        if let fontStyle = fontStyle {
            Theme.shared.setFontStyle(selectedFont: fontStyle)
        }
        
        networkHelper.startObserver()
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            let user = Auth.auth().currentUser
            if let user = user {
                userEmail = user.email
            }
            print("\(userEmail!) is signed in")
        } else {
            print("User is not signed in")
        }
        
        // Do any additional setup after loading the view.
        journalCollectionView.delegate = self
        journalCollectionView.dataSource = self
        
        db = Firestore.firestore()
        loadDatabaseEntries()
        
        lastOrientation = UIDevice.current.orientation
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        //        do{
        //            try reachability.startNotifier()
        //        }catch{
        //            print("could not start reachability notifier")
        //        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setCollectionViewLayout()
        loadTheme()
        loadDatabaseEntries()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == textSegueIdentifier {
            let detailVC = segue.destination as! TextDetailVC
            let cell = sender as! TextCVC
            let indexPath = self.journalCollectionView.indexPath(for: cell)
            let selectedEntry = self.entriesArray[indexPath!.row] as JournalEntry
            detailVC.selectedEntry = selectedEntry
        } else if segue.identifier == photoSegueIdentifier {
            let detailVC = segue.destination as! PhotoDetailVC
            let cell = sender as! PhotoCVC
            let indextPath = self.journalCollectionView.indexPath(for: cell)
            let selectedEntry = self.entriesArray[indextPath!.row] as JournalEntry
            detailVC.selectedEntry = selectedEntry
        } else if segue.identifier == audioSegueIdentifier {
            let detailVC = segue.destination as! AudioDetailVC
            let cell = sender as! AudioCVC
            let indexPath = self.journalCollectionView.indexPath(for: cell)
            let selectedEntry = self.entriesArray[indexPath!.row] as JournalEntry
            detailVC.selectedEntry = selectedEntry
        }
    }
    
    @IBAction func didPressBarButton(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            print("Text tapped")
            if let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "textEntryVC") {
                modalAnimation(vc2: vc2)
            }
        case 1:
            print("Media tapped")
            if let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "photoEntryVC") {
                modalAnimation(vc2: vc2)
            }
        case 2:
            print("Audio tapped")
            if let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "audioEntryVC") {
                vc2.hero.isEnabled = true
                
                vc2.hero.modalAnimationType = .selectBy(presenting: .fade, dismissing: .fade)
                
                present(vc2, animated: true, completion: nil)
            }
        default:
            return
        }
    }
    
    func modalAnimation(vc2: UIViewController) {
        vc2.hero.isEnabled = true
        
        vc2.hero.modalAnimationType = .selectBy(presenting: .zoom, dismissing: .zoomOut)
        
        self.present(vc2, animated: true, completion: nil)
    }
    
    // Load the current theme
    func loadTheme() {
        customNavBar.backgroundColor = Theme.shared.getThemecolor()
        self.tabBarController?.tabBar.tintColor = Theme.shared.getThemecolor()
    }
    
    func setCollectionViewLayout() {
        let layout = journalCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let width = journalCollectionView.frame.size.width
        let xInsets: CGFloat = 10
        let cellSpacing: CGFloat = 5
        let numberOfColumns: CGFloat = 2
        
        layout.estimatedItemSize = CGSize(width: (width/numberOfColumns) - (xInsets + cellSpacing), height: 40)
    }
    
    @objc func rotated() {
        
        let currentOrientation = UIDevice.current.orientation
        
        guard UIDeviceOrientationIsLandscape(currentOrientation) || UIDeviceOrientationIsPortrait(currentOrientation) else {
            return
        }
        
        guard currentOrientation != lastOrientation else {
            return
        }
        
        lastOrientation = currentOrientation
        print(currentOrientation.isLandscape)
        
        setCollectionViewLayout()
    }
    
    //    @objc func reachabilityChanged(note: Notification) {
    //
    //        let reachability = note.object as! Reachability
    //
    //        switch reachability.connection {
    //        case .wifi:
    //            print("Reachable via WiFi")
    //            networkConnected = true
    //        case .cellular:
    //            print("Reachable via Cellular")
    //            networkConnected = true
    //        case .none:
    //            print("Network not reachable")
    //            networkConnected = false
    //        }
    //    }
}

extension JournalVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return entriesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let journalCell = collectionView.dequeueReusableCell(withReuseIdentifier: textCellIdentifier, for: indexPath) as! TextCVC
        
        setCellDesign(journalCell: journalCell)
        
        let entry = entriesArray[indexPath.item]
        
        if entry.photoURL != nil {
            let journalCell = collectionView.dequeueReusableCell(withReuseIdentifier: photoCellIdentifier, for: indexPath) as! PhotoCVC
            
            setCellDesign(journalCell: journalCell)
            
            journalCell.entryLabel.text = entry.textEntry
            journalCell.dateLabel.text = entry.dateEntered
            
            if let photoURL = URL(string: entry.photoURL!) {
                if let data = try? Data(contentsOf: photoURL) {
                    DispatchQueue.global().async {
                        DispatchQueue.main.async {
                            journalCell.photoView.image = UIImage(data: data)
                        }
                    }
                }
            }
            
            journalCell.entryLabel.font = Theme.shared.getFontStyle()
            
            return journalCell
        } else if entry.audioURL != nil {
            let journalCell = collectionView.dequeueReusableCell(withReuseIdentifier: audioCellIdentifier, for: indexPath) as! AudioCVC
            
            setCellDesign(journalCell: journalCell)
            
            if let textEntry = entry.textEntry {
                journalCell.entryLabel.text = textEntry
                print(textEntry)
                
                journalCell.entryLabel.font = Theme.shared.getFontStyle()
            }
            
            journalCell.dateLabel.text = entry.dateEntered
            
            return journalCell
        }
        
        journalCell.entryLabel.text = entry.textEntry
        journalCell.dateLabel.text = entry.dateEntered
        
        journalCell.entryLabel.font = Theme.shared.getFontStyle()
        
        return journalCell
    }
    
    func loadDatabaseEntries() {
        
        if networkHelper.isConnected() {
            db.collection("users").document(userEmail!).collection("journal entries").getDocuments { (snapshot, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    self.entriesArray.removeAll()
                    for document in snapshot!.documents {
                        print("\n")
                        let docId = document.documentID
                        let textEntry = document.get("entry") as? String
                        let downloadURL = document.get("downloadURL") as? String
                        let audioURL = document.get("audioURL") as? String
                        let latitude = document.get("latitude") as? Double
                        let longitude = document.get("longitude") as? Double
                        let journalEntry = JournalEntry.init(dateEntered: docId, textEntry: textEntry, downloadURL: downloadURL, audioURL: audioURL, latitude: latitude, longitude: longitude)
                        self.entriesArray.append(journalEntry)
                    } // end for loop
                } // end if statement
                DispatchQueue.main.async {
                    if self.entriesArray.count == 0 {
                        self.journalCollectionView.isHidden = true
                    } else {
                        self.journalCollectionView.isHidden = false
                        self.journalCollectionView.reloadData()
                        self.journalCollectionView.reloadSections([0])
                    }
                }
                self.databaseHelper.setCachedentries(entries: self.entriesArray)
            } // end db closure
        } else {
            networkHelper.showNetworkAlert(view: self)
            entriesArray = databaseHelper.getCachedEntries()
        }
    } // end function
    
    func setCellDesign(journalCell: UICollectionViewCell) {
        journalCell.layer.cornerRadius = 10
    }
}


