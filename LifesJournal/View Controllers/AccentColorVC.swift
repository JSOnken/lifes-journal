//
//  AccentColorVC.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/10/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import DLRadioButton

class AccentColorVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    
    // MARK: - Properties
    var accentColor = ""
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        modalView.applyViewDesign()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        Theme.shared.setThemeColor(selectedColor: accentColor)
        let defaults = UserDefaults.standard
        defaults.set(accentColor, forKey: "AccentColor")
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelbuttonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        accentColor = radioButton.selected()?.titleLabel?.text ?? ""
        saveButton.isEnabled = true
        
        print("\(accentColor) is selected.\n")
    }
}
