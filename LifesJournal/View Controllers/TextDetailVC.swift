//
//  TextDetailVC.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/30/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase

class TextDetailVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var entryTV: UITextView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var shareButton: UIButton!
    
    
    // MARK: - Properties
    var db: Firestore!
    let networkHelper: NetworkHelper = NetworkHelper.sharedInstance
    var selectedEntry: JournalEntry!
    var userEmail: String?
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        db = Firestore.firestore()
        
        entryTV.isEditable = false
        entryTV.text = selectedEntry.textEntry
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            let user = Auth.auth().currentUser
            if let user = user {
                userEmail = user.email
            }
        } else {
            print("User is not signed in")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadTheme()
        entryTV.font = Theme.shared.getFontStyle()
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        if networkHelper.isConnected() {
            db.collection("users").document(userEmail!).collection("journal entries").document(selectedEntry.dateEntered!).delete { (error) in
                if let error = error {
                    print("Error deleting document: \(error.localizedDescription)")
                } else {
                    print("Document was successfully deleted")
                    self.dismiss(animated: true, completion: nil)
                }
            }
        } else {
            networkHelper.showNetworkAlert(view: self)
        }
    }
    
    @IBAction func editButtonPressed(_ sender: Any) {
        saveButton.isHidden = false
        editButton.isHidden = true
        entryTV.isEditable = true
        entryTV.setNeedsFocusUpdate()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        guard let textEntry = entryTV.text, !textEntry.isEmpty else {return}
        
        if networkHelper.isConnected() {
            db.collection("users").document(userEmail!).collection("journal entries").document(selectedEntry.dateEntered!)
                .setData(["entry":textEntry], merge: true, completion: { (error) in
                    if let error = error {
                        print("Error writing document: \(error.localizedDescription)")
                    } else {
                        print("Document successfully written!")
                        self.dismiss(animated: true)
                    }
                })
        } else {
            networkHelper.showNetworkAlert(view: self)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        let items = [selectedEntry.textEntry!]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
        if let popOver = ac.popoverPresentationController {
            popOver.sourceView = self.shareButton
        }
    }
    
    func loadTheme() {
        customNavBar.backgroundColor = Theme.shared.getThemecolor()
    }
}
