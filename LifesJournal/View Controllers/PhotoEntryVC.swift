//
//  PhotoEntryViewController.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/18/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase
import MobileCoreServices
import CoreLocation

class PhotoEntryVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, CLLocationManagerDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var photoEntryImageView: UIImageView?
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var openPhotosButton: UIButton!
    @IBOutlet weak var openCameraButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var customNavBar: UIView!
    
    // MARK: - Properties
    var db: Firestore!
    let networkHelper: NetworkHelper = NetworkHelper.sharedInstance
    let locationManager = CLLocationManager()
    let imagePicker = UIImagePickerController()
    var imageData: NSData?
    var movieData: NSURL?
    var userID: String?
    var userEmail: String?
    var locationAuthorized: Bool?
    var currentLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AudioEntryVC.dismissKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(PhotoEntryVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PhotoEntryVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        db = Firestore.firestore()
        
        openPhotosButton.applyButtonDesign()
        openCameraButton.applyButtonDesign()
        
        imagePicker.delegate = self
        descriptionTextView.delegate = self
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            print("User is signed in")
            let user = Auth.auth().currentUser
            if let user = user {
                userID = user.uid
                userEmail = user.email
            }
        } else {
            print("User is not signed in")
        }
        
        descriptionTextView.layer.cornerRadius = 5
        descriptionTextView.layer.shadowColor = UIColor.gray.cgColor
        descriptionTextView.layer.shadowRadius = 3
        descriptionTextView.layer.shadowOpacity = 0.5
        descriptionTextView.layer.shadowOffset = CGSize(width: 0, height: 0)
        descriptionTextView.layer.masksToBounds = false
        
        checkLocationAuthorizationStatus()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadTheme()
        descriptionTextView.font = Theme.shared.getFontStyle()
    }
    
    // Function for setting the user selected image to the entry
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let mediaType: String = info[UIImagePickerControllerMediaType] as? String else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        if mediaType == (kUTTypeImage as String) {
            if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage,
                let imageData = UIImageJPEGRepresentation(selectedImage, 0.8) {
                photoEntryImageView?.contentMode = .scaleAspectFit
                photoEntryImageView?.image = selectedImage
                self.imageData = imageData as NSData
            } else if mediaType == (kUTTypeMovie as String) {
                if let movieUrl = info[UIImagePickerControllerMediaURL] as? NSURL {
                    self.movieData = movieUrl
                }
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    // Function for dismissing the controller
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openCameraButtonPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func openPhotosButtonPressed(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if photoEntryImageView?.image != nil {
            if imageData != nil {
                uploadImageToFirebaseStorage(data: imageData!)
            } else if movieData != nil {
                
            }
        } else {
            networkHelper.showNetworkAlert(view: self)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func uploadImageToFirebaseStorage(data: NSData) {
        let textEntry = descriptionTextView.text!
        
        let storageRef = Storage.storage().reference(withPath: "/Photos/\(userEmail!)/\(getDateTime()).jpg")
        let uploadMetaData = StorageMetadata()
        
        uploadMetaData.contentType = "image/jpg"
        
        let uploadTask = storageRef.putData(data as Data, metadata: uploadMetaData) { (metaData, error) in
            if let error = error {
                print("Error uploading photo: \(error.localizedDescription)")
            } else {
                // Get the download url from the image after it was uploaded
                storageRef.downloadURL(completion: { (url, error) in
                    // Pring the error if one is found
                    if let error = error {
                        print("Error getting URL: \(error.localizedDescription)")
                        // Get the download URL if one is found
                    } else {
                        if let downloadURL = url?.absoluteString {
                            print("URL: \(downloadURL)")
                            // If we do not have authorization to user location
                            if self.locationAuthorized == false {
                                // Set the data for the photo entry to the database without GPS location
                                self.db.collection("users").document(self.userEmail!).collection("journal entries").document(self.getDateTime())
                                    .setData(["entry":textEntry,
                                              "downloadURL":downloadURL], merge: true, completion: { (error) in
                                                if let error = error {
                                                    print("Error writing document: \(error.localizedDescription)")
                                                } else {
                                                    print("Document successfully written!")
                                                    self.dismiss(animated: true)
                                                }
                                    })
                                // If we do have authorization to user location
                            } else if self.locationAuthorized == true {
                                // Set the data for the photo entry to the database with GPS location
                                self.db.collection("users").document(self.userEmail!).collection("journal entries").document(self.getDateTime())
                                    .setData(["entry":textEntry,
                                              "latitude": self.currentLocation!.coordinate.latitude,
                                              "longitude": self.currentLocation!.coordinate.longitude,
                                              "downloadURL":downloadURL], merge: true, completion: { (error) in
                                                if let error = error {
                                                    print("Error writing document: \(error.localizedDescription)")
                                                } else {
                                                    print("Document successfully written!")
                                                    self.dismiss(animated: true)
                                                }
                                    }) // end of db completion block
                            } // end of locationAuthorized if check
                        } // end of download url if let block
                    } // end of else block
                }) // end of downloadURL completion block
            }
        } // end of upload task
        
        // Update progress bar
        uploadTask.observe(.progress) { [weak self] (snapshot) in
            guard let progress = snapshot.progress else { return }
            self?.progressView.isHidden = false
            self?.progressView.progress = Float(progress.fractionCompleted)
        }
    }
    
    func uploadMovieToFirebaseStorage(url: NSURL) {
        let textEntry = descriptionTextView.text!
        
        let storageRef = Storage.storage().reference(withPath: "/Movies/\(userEmail!)/\(getDateTime()).mov")
        let uploadMetaData = StorageMetadata()
        
        uploadMetaData.contentType = "video/quicktime"
        uploadMetaData.customMetadata = ["entry":textEntry]
        
        storageRef.putFile(from: url as URL, metadata: uploadMetaData) { (metaData, error) in
            if let error = error {
                print("Error uploading photo: \(error.localizedDescription)")
            } else {
                print("Meta data uploaded. Metada: \(String(describing: metaData))")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func getDateTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        let dateString = formatter.string(from: Date())
        return dateString
    }
    
    func checkLocationAuthorizationStatus() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationAuthorized = true
            break
        case .denied:
            locationAuthorized = false
            break
        case .restricted:
            locationAuthorized = false
            break
        default:
            locationManager.requestWhenInUseAuthorization()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locationValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        currentLocation = CLLocation(latitude: locationValue.latitude, longitude: locationValue.longitude)
    }
    
    func loadTheme() {
        customNavBar.backgroundColor = Theme.shared.getThemecolor()
        openCameraButton.backgroundColor = Theme.shared.getThemecolor()
        openPhotosButton.backgroundColor = Theme.shared.getThemecolor()
    }
}

extension PhotoEntryVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    @objc func dismissKeyboard() {
        descriptionTextView.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 200
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 200
            }
        }
    }
}
