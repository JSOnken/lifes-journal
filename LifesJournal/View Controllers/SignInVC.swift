//
//  SignInViewController.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/14/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Hero
import Firebase
import FacebookCore
import FacebookLogin

class SignInVC: UIViewController {

    @IBOutlet var facebookSignInButton: UIButton!
    @IBOutlet weak var emailSignInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        facebookSignInButton.applyButtonDesign()
        emailSignInButton.applyButtonDesign()
        
        if Auth.auth().currentUser != nil {
            print("User is signed in")
            if let tabViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController {
                
                tabViewController.hero.isEnabled = true
                
                tabViewController.hero.modalAnimationType = .selectBy(presenting: .zoomOut, dismissing: .fade)
                
                self.present(tabViewController, animated: true, completion: nil)
            }
        } else {
            print("User is not signed in")
        }
    }
    
    @IBAction func facebookButtonPressed(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [ .email ], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print("Error failed: \(error)")
            case .cancelled:
                print("User cancelled login")
            case .success( _, _, let accessToken):
                print("Logged in!")
                let credentials = FacebookAuthProvider.credential(withAccessToken: accessToken.authenticationToken)
                Auth.auth().signInAndRetrieveData(with: credentials, completion: { (authResult, error) in
                    if let error = error {
                        print("Error with Facebook token: \(error.localizedDescription)")
                        return
                    }
                    print("User is signed in!")
                    if let tabViewController = self.storyboard?.instantiateViewController(withIdentifier: "tabBarController") as? UITabBarController {
                        
                        tabViewController.hero.isEnabled = true
                        
                        tabViewController.hero.modalAnimationType = .selectBy(presenting: .zoomOut, dismissing: .fade)
                        
                        self.present(tabViewController, animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    @IBAction func emailButtonPressed(_ sender: Any) {
        if let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "signInPopupVC") {
            
            vc2.hero.isEnabled = true
            vc2.hero.modalAnimationType = .selectBy(presenting: .fade, dismissing: .fade)
            
            present(vc2, animated: true, completion: nil)
        }
    }
}

extension UIButton {
    func applyButtonDesign() {
        self.setTitleColor(UIColor.white, for: .normal)
        self.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.masksToBounds = false
    }
}

extension UIView {
    func applyViewDesign() {
        self.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowRadius = 7
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.masksToBounds = false
    }
}
