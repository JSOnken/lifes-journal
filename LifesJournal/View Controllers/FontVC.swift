//
//  FontVC.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/10/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import DLRadioButton

class FontVC: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    
    // MARK: - Properties
    var fontStyle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        modalView.applyViewDesign()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        Theme.shared.setFontStyle(selectedFont: fontStyle)
        let defaults = UserDefaults.standard
        defaults.set(fontStyle, forKey: "FontStyle")
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        fontStyle = radioButton.selected()?.titleLabel?.text ?? ""
        saveButton.isEnabled = true
        
        print("\(fontStyle) is selected.\n")
    }
}
