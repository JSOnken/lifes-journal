//
//  CalendarViewController.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/13/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Firebase

private let textCellIdentifier = "TextEntryIdentifier"
private let photoCellIdentifier = "PhotoEntryIdentifier"
private let audioCellIdentifier = "AudioEntryIdentifier"
private let photoSegueIdentifier = "photoDetailSegueIdentifier"
private let textSegueIdentifier = "textDetailSegueIdentifier"
private let audioSegueIdentifier = "audioDetailSegueIdentifier"

class CalendarVC: UIViewController, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var customNavBar: UIView!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var journalCollectionView: UICollectionView!
    
    var db: Firestore!
    let networkHelper: NetworkHelper = NetworkHelper.sharedInstance
    let databaseHelper: DatabaseHelper = DatabaseHelper.sharedInstance
    var userEmail: String?
    var entriesArray = [JournalEntry]()
    var selectedDateArray = [JournalEntry]()
    let formatter = DateFormatter()
    let monthColor = UIColor.white
    let selectedColor = UIColor.darkGray
    let outsideMonthColor = UIColor.gray
    var helper: DatabaseHelper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            let user = Auth.auth().currentUser
            if let user = user {
                userEmail = user.email
            }
            print("\(userEmail!) is signed in")
        } else {
            print("User is not signed in")
        }
        
        // Do any additional setup after loading the view.
        setupCalendarView()
        helper = DatabaseHelper()
        db = Firestore.firestore()
        loadDatabaseEntries()
        
        setCollectionViewLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setCollectionViewLayout()
        loadTheme()
        journalCollectionView.reloadData()
    }
    
    func setupCalendarView() {
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        
        calendarView.visibleDates { (visibleDates) in
            self.calendarView.scrollToDate(Date())
            self.setupViewsOfCalendar(from: visibleDates)
        }
    }
    
    func setCellSelected(view: JTAppleCell?, cellState: CellState) {
        guard let selectedCell = view as? CalendarCell else {return}
        
        if cellState.isSelected {
            selectedCell.selectedView.isHidden = false
        } else {
            selectedCell.selectedView.isHidden = true
        }
    }
    
    func setCellTextColor(view: JTAppleCell?, cellState: CellState) {
        guard let validCell = view as? CalendarCell else {return}
        
        if cellState.isSelected {
            validCell.dateLabel.textColor = selectedColor
        } else {
            if cellState.dateBelongsTo == .thisMonth {
                validCell.dateLabel.textColor = monthColor
            } else {
                validCell.dateLabel.textColor = outsideMonthColor
            }
        }
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        
        formatter.dateFormat = "MMMM yyyy"
        monthYearLabel.text = formatter.string(from: date)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadTheme() {
        customNavBar.backgroundColor = Theme.shared.getThemecolor()
        journalCollectionView.backgroundColor = Theme.shared.getThemecolor()
    }
    
    func loadDatabaseEntries() {
        
        if networkHelper.isConnected() {
            db.collection("users").document(userEmail!).collection("journal entries").getDocuments { (snapshot, error) in
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    self.entriesArray.removeAll()
                    for document in snapshot!.documents {
                        print("\n")
                        let docId = document.documentID
                        let textEntry = document.get("entry") as? String
                        let downloadURL = document.get("downloadURL") as? String
                        let audioURL = document.get("audioURL") as? String
                        let latitude = document.get("latitude") as? Double
                        let longitude = document.get("longitude") as? Double
                        let journalEntry = JournalEntry.init(dateEntered: docId, textEntry: textEntry, downloadURL: downloadURL, audioURL: audioURL, latitude: latitude, longitude: longitude)
                        self.entriesArray.append(journalEntry)
                    } // end for loop
                } // end if statement
            } // end db closure
        } else {
            networkHelper.showNetworkAlert(view: self)
            entriesArray = databaseHelper.getCachedEntries()
        }
    } // end function
    
    func getSelectedDateEntries(date: String) {
        selectedDateArray.removeAll()
        for entry in entriesArray {
            let dateTimeString = entry.dateEntered
            let splitString = dateTimeString?.split(separator: " ", maxSplits: 1, omittingEmptySubsequences: false)
            let dateString = splitString![0]
            
            if dateString == date {
                selectedDateArray.append(entry)
            }
        }
        
        journalCollectionView.reloadData()
    }
}

extension CalendarVC: JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startDate = formatter.date(from: "2018 01 01")!
        let endDate = formatter.date(from: "2020 12 31")!
        let calendar = Calendar.current
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: 6,
                                                 calendar: calendar,
                                                 generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfGrid,
                                                 firstDayOfWeek: .sunday)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        let customCell = cell as! CalendarCell
        customCell.dateLabel.text = cellState.text
        
        
    }
}

extension CalendarVC: JTAppleCalendarViewDelegate {
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let customCell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "calendarCell", for: indexPath) as! CalendarCell
        
        customCell.entryFoundView.isHidden = true
        customCell.dateLabel.text = cellState.text
        
        setCellSelected(view: customCell, cellState: cellState)
        setCellTextColor(view: customCell, cellState: cellState)
        
        for entry in entriesArray {
            let dateTimeString = entry.dateEntered
            let splitString = dateTimeString?.split(separator: " ", maxSplits: 1, omittingEmptySubsequences: false)
            let dateString = splitString![0]
            
            formatter.dateFormat = "MM-dd-yyyy"
            let calendarDateString = formatter.string(from: date)
            
            if dateString == calendarDateString {
                customCell.entryFoundView.isHidden = false
            }
        }
        
        return customCell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        setCellSelected(view: cell, cellState: cellState)
        setCellTextColor(view: cell, cellState: cellState)
        
        formatter.dateFormat = "MM-dd-yyyy"
        let dateString = formatter.string(from: date)
        getSelectedDateEntries(date: dateString)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        setCellSelected(view: cell, cellState: cellState)
        setCellTextColor(view: cell, cellState: cellState)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == textSegueIdentifier {
            let detailVC = segue.destination as! TextDetailVC
            let cell = sender as! TextCVC
            let indexPath = self.journalCollectionView.indexPath(for: cell)
            let selectedEntry = self.selectedDateArray[indexPath!.row] as JournalEntry
            detailVC.selectedEntry = selectedEntry
        } else if segue.identifier == photoSegueIdentifier {
            let detailVC = segue.destination as! PhotoDetailVC
            let cell = sender as! PhotoCVC
            let indextPath = self.journalCollectionView.indexPath(for: cell)
            let selectedEntry = self.selectedDateArray[indextPath!.row] as JournalEntry
            detailVC.selectedEntry = selectedEntry
        } else if segue.identifier == audioSegueIdentifier {
            let detailVC = segue.destination as! AudioDetailVC
            let cell = sender as! AudioCVC
            let indexPath = self.journalCollectionView.indexPath(for: cell)
            let selectedEntry = self.selectedDateArray[indexPath!.row] as JournalEntry
            detailVC.selectedEntry = selectedEntry
        }
    }
    
    func setCollectionViewLayout() {
        let layout = journalCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let width = journalCollectionView.frame.size.width
        let xInsets: CGFloat = 10
        let cellSpacing: CGFloat = 5
        let numberOfColumns: CGFloat = 2
        
        layout.estimatedItemSize = CGSize(width: (width/numberOfColumns) - (xInsets + cellSpacing), height: 40)
    }
}

extension CalendarVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedDateArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let journalCell = collectionView.dequeueReusableCell(withReuseIdentifier: textCellIdentifier, for: indexPath) as! TextCVC
        
        setCellDesign(journalCell: journalCell)
        
        let entry = selectedDateArray[indexPath.item]
        
        if entry.photoURL != nil {
            let journalCell = collectionView.dequeueReusableCell(withReuseIdentifier: photoCellIdentifier, for: indexPath) as! PhotoCVC
            
            setCellDesign(journalCell: journalCell)
            
            journalCell.entryLabel.text = entry.textEntry
            journalCell.dateLabel.text = entry.dateEntered
            
            if let photoURL = URL(string: entry.photoURL!) {
                if let data = try? Data(contentsOf: photoURL) {
                    DispatchQueue.global().async {
                        DispatchQueue.main.async {
                            journalCell.photoView.image = UIImage(data: data)
                        }
                    }
                }
            }
            
            journalCell.entryLabel.font = Theme.shared.getFontStyle()
            
            return journalCell
        } else if entry.audioURL != nil {
            let journalCell = collectionView.dequeueReusableCell(withReuseIdentifier: audioCellIdentifier, for: indexPath) as! AudioCVC
            
            setCellDesign(journalCell: journalCell)
            
            if let textEntry = entry.textEntry {
                journalCell.entryLabel.text = textEntry
                print(textEntry)
                
                journalCell.entryLabel.font = Theme.shared.getFontStyle()
            }
            
            journalCell.dateLabel.text = entry.dateEntered
            
            return journalCell
        }
        
        journalCell.entryLabel.text = entry.textEntry
        journalCell.dateLabel.text = entry.dateEntered
        
        journalCell.entryLabel.font = Theme.shared.getFontStyle()
        
        return journalCell
    }
    
    func setCellDesign(journalCell: UICollectionViewCell) {
        journalCell.layer.cornerRadius = 10
    }
}
