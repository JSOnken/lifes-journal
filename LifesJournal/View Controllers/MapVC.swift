//
//  MapViewController.swift
//  LifesJournal
//
//  Created by Jason Onken on 9/26/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Firebase

private let textCellIdentifier = "TextEntryIdentifier"
private let photoCellIdentifier = "PhotoEntryIdentifier"
private let audioCellIdentifier = "AudioEntryIdentifier"
private let photoSegueIdentifier = "photoDetailSegueIdentifier"
private let textSegueIdentifier = "textDetailSegueIdentifier"
private let audioSegueIdentifier = "audioDetailSegueIdentifier"

class MapVC: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var journalCollectionView: UICollectionView!
    
    // MARK: - Variables
    var db: Firestore!
    let networkHelper: NetworkHelper = NetworkHelper.sharedInstance
    let databaseHelper: DatabaseHelper = DatabaseHelper.sharedInstance
    let locationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 350
    var entriesArray = [JournalEntry]()
    var selectedPinArray = [JournalEntry]()
    var locationAuthorized = false
    var userEmail: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Auth.auth().currentUser != nil {
            // User is signed in.
            let user = Auth.auth().currentUser
            if let user = user {
                userEmail = user.email
            }
            print("\(userEmail!) is signed in")
        } else {
            print("User is not signed in")
        }
        
        // Do any additional setup after loading the view.
        db = Firestore.firestore()
        loadDatabaseEntries()
        
        checkLocationAuthorizationStatus()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        } else {
            print("We are not authorized")
        }
        
        mapView.delegate = self
        journalCollectionView.delegate = self
        journalCollectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setCollectionViewLayout()
        loadTheme()
        journalCollectionView.reloadData()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locationValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        let currentLocation = CLLocation(latitude: locationValue.latitude, longitude: locationValue.longitude)
        centerMapOnLocation(location: currentLocation)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
            locationAuthorized = true
        } else if CLLocationManager.authorizationStatus() != .denied ||
            CLLocationManager.authorizationStatus() != .restricted {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if let userPin = mapView.view(for: mapView.userLocation) {
            userPin.isHidden = true
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "customAnnotation")
        annotationView.image = UIImage(named: "location_pin")
        annotationView.canShowCallout = true
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        selectedPinArray.removeAll()
        for entry in entriesArray {
            let latitude = view.annotation?.coordinate.latitude
            let longitude = view.annotation?.coordinate.longitude
            
            if entry.latitude == latitude && entry.longitude == longitude {
                selectedPinArray.append(entry)
            }
        }
        
        journalCollectionView.reloadData()
    }
    
    func getMapPins() {
        for entry in entriesArray {
            if entry.latitude != nil && entry.longitude != nil {
                let pinLocation = CLLocationCoordinate2D(latitude: entry.latitude!, longitude: entry.longitude!)
                
                let pin = CustomPin.init(title: "Journal", subtitle: "Found some entries!", location: pinLocation)
                
                mapView.addAnnotation(pin)
            }
        }
    }
    
    func loadDatabaseEntries() {
        if networkHelper.isConnected() {
            entriesArray = databaseHelper.getDatabaseEntries(database: db)
            DispatchQueue.main.async {
                self.getMapPins()
            }
        } else {
            networkHelper.showNetworkAlert(view: self)
            entriesArray = databaseHelper.getCachedEntries()
            DispatchQueue.main.async {
                self.getMapPins()
            }
        }
    } // end function
    
    // Load the current theme
    func loadTheme() {
        journalCollectionView.backgroundColor = Theme.shared.getThemecolor()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == textSegueIdentifier {
            let detailVC = segue.destination as! TextDetailVC
            let cell = sender as! TextCVC
            let indexPath = self.journalCollectionView.indexPath(for: cell)
            let selectedEntry = self.selectedPinArray[indexPath!.row] as JournalEntry
            detailVC.selectedEntry = selectedEntry
        } else if segue.identifier == photoSegueIdentifier {
            let detailVC = segue.destination as! PhotoDetailVC
            let cell = sender as! PhotoCVC
            let indextPath = self.journalCollectionView.indexPath(for: cell)
            let selectedEntry = self.selectedPinArray[indextPath!.row] as JournalEntry
            detailVC.selectedEntry = selectedEntry
        } else if segue.identifier == audioSegueIdentifier {
            let detailVC = segue.destination as! AudioDetailVC
            let cell = sender as! AudioCVC
            let indexPath = self.journalCollectionView.indexPath(for: cell)
            let selectedEntry = self.selectedPinArray[indexPath!.row] as JournalEntry
            detailVC.selectedEntry = selectedEntry
        }
    }
    
    func setCollectionViewLayout() {
        let layout = journalCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let width = journalCollectionView.frame.size.width
        let xInsets: CGFloat = 10
        let cellSpacing: CGFloat = 5
        let numberOfColumns: CGFloat = 2
        
        layout.estimatedItemSize = CGSize(width: (width/numberOfColumns) - (xInsets + cellSpacing), height: 40)
    }
}

extension MapVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedPinArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let journalCell = collectionView.dequeueReusableCell(withReuseIdentifier: textCellIdentifier, for: indexPath) as! TextCVC
        
        setCellDesign(journalCell: journalCell)
        
        let entry = selectedPinArray[indexPath.item]
        
        if entry.photoURL != nil {
            let journalCell = collectionView.dequeueReusableCell(withReuseIdentifier: photoCellIdentifier, for: indexPath) as! PhotoCVC
            
            setCellDesign(journalCell: journalCell)
            
            journalCell.entryLabel.text = entry.textEntry
            journalCell.dateLabel.text = entry.dateEntered
            
            if let photoURL = URL(string: entry.photoURL!) {
                if let data = try? Data(contentsOf: photoURL) {
                    DispatchQueue.global().async {
                        DispatchQueue.main.async {
                            journalCell.photoView.image = UIImage(data: data)
                        }
                    }
                }
            }
            
            journalCell.entryLabel.font = Theme.shared.getFontStyle()
            
            return journalCell
        } else if entry.audioURL != nil {
            let journalCell = collectionView.dequeueReusableCell(withReuseIdentifier: audioCellIdentifier, for: indexPath) as! AudioCVC
            
            setCellDesign(journalCell: journalCell)
            
            if let textEntry = entry.textEntry {
                journalCell.entryLabel.text = textEntry
                print(textEntry)
                
                journalCell.entryLabel.font = Theme.shared.getFontStyle()
            }
            
            journalCell.dateLabel.text = entry.dateEntered
            
            return journalCell
        }
        
        journalCell.entryLabel.text = entry.textEntry
        journalCell.dateLabel.text = entry.dateEntered
        
        journalCell.entryLabel.font = Theme.shared.getFontStyle()
        
        return journalCell
    }
    
    func setCellDesign(journalCell: UICollectionViewCell) {
        journalCell.layer.cornerRadius = 10
    }
}
