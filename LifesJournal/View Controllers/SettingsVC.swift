//
//  SettingsVC.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/8/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import Firebase

class SettingsVC: UITableViewController {

    @IBOutlet weak var signOutCell: UITableViewCell!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        if Auth.auth().currentUser != nil {
            // User is signed in.
            let user = Auth.auth().currentUser
            if let user = user {
                let userEmail = user.email
                emailLabel.text = userEmail
                print("\(userEmail!) is signed in")
            }
        } else {
            print("User is not signed in")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadTheme()
        print("Settings will appear")
    }
    @IBAction func signOutPressed(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            print("Signing out")
        } catch let signOutError as NSError {
            print ("Error signing out: \(signOutError)")
        }
    }
    
    func loadTheme() {
        self.tabBarController?.tabBar.tintColor = Theme.shared.getThemecolor()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 2 {
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                print("Signing out")
                if let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "signInVC") {
                    
                    vc2.hero.isEnabled = true
                    vc2.hero.modalAnimationType = .selectBy(presenting: .fade, dismissing: .fade)
                    
                    present(vc2, animated: true, completion: nil)
                }
            } catch let signOutError as NSError {
                print ("Error signing out: \(signOutError)")
            }
        }
    }
}
