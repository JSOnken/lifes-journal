//
//  CustomPin.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/4/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import UIKit
import MapKit

class CustomPin: NSObject, MKAnnotation {

    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(title: String, subtitle: String, location: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = location
    }
}
