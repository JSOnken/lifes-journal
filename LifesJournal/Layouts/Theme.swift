//
//  Theme.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/11/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import UIKit

class Theme {
    
    static let shared = Theme()
    
    var themeColor = UIColor(red: 65/255, green: 169/255, blue: 72/255, alpha: 1.0)
    
    let blueColor = UIColor(red: 33/255, green: 85/255, blue: 111/255, alpha: 1.0)
    let darkColor = UIColor(red: 28/255, green: 42/255, blue: 51/255, alpha: 1.0)
    let greenColor = UIColor(red: 65/255, green: 169/255, blue: 72/255, alpha: 1.0)
    let redColor = UIColor(red: 159/255, green: 34/255, blue: 20/255, alpha: 1.0)
    
    private init(){}
    
    func setThemeColor(selectedColor: String) {
        switch selectedColor.lowercased() {
        case "blue":
            themeColor = blueColor
            break
        case "dark":
            themeColor = darkColor
            break
        case "green":
            themeColor = greenColor
            break
        case "red":
            themeColor = redColor
            break
        default:
            themeColor = greenColor
        }
    }
    
    func getThemecolor() -> UIColor {
        return themeColor
    }
    
    var fontStyle: UIFont?
    
    let americanTypwriterFont = UIFont(name: "American Typewriter", size: 18)
    let chalkboardSEFont = UIFont(name: "Chalkboard SE", size: 18)
    let noteworthyFont = UIFont(name: "Noteworthy", size: 18)
    let papyrusFont = UIFont(name: "Papyrus", size: 18)
    let defaultFont = UIFont(name: "System", size: 18)
    
    func setFontStyle(selectedFont: String) {
        switch selectedFont {
        case "American Typewriter":
            fontStyle = americanTypwriterFont
            break
        case "Chalkboard SE":
            fontStyle = chalkboardSEFont
            break
        case "Noteworthy":
            fontStyle = noteworthyFont
            break
        case "Papyrus":
            fontStyle = papyrusFont
            break
        default:
            fontStyle = nil
        }
    }
    
    func getFontStyle() -> UIFont? {
        return fontStyle
    }
}
