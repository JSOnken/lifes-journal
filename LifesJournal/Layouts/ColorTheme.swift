//
//  ColorTheme.swift
//  LifesJournal
//
//  Created by Jason Onken on 10/11/18.
//  Copyright © 2018 JasonOnken. All rights reserved.
//

import Foundation
import UIKit

class ColorTheme {
    
    static let shared = ColorTheme()
    
    var themeColor = UIColor(red: 65/255, green: 169/255, blue: 72/255, alpha: 1.0)
    
    let blueColor = UIColor(red: 33/255, green: 85/255, blue: 111/255, alpha: 1.0)
    let darkColor = UIColor(red: 28/255, green: 42/255, blue: 51/255, alpha: 1.0)
    let greenColor = UIColor(red: 65/255, green: 169/255, blue: 72/255, alpha: 1.0)
    let redColor = UIColor(red: 159/255, green: 34/255, blue: 20/255, alpha: 1.0)
    
    private init(){}
    
    func setThemeColor(selectedColor: String) {
        switch selectedColor {
        case "blue":
            themeColor = blueColor
            break
        case "dark":
            themeColor = darkColor
            break
        case "green":
            themeColor = greenColor
            break
        case "red":
            themeColor = redColor
            break
        default:
            themeColor = greenColor
        }
    }
    
    func getThemecolor() -> UIColor {
        return themeColor
    }
}
